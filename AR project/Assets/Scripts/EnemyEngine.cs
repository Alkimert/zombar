﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyEngine : MonoBehaviour
{
    [SerializeField] private GameObject pfEnemy;
    [SerializeField] public GameObject Target;
    [SerializeField] private bool aimTarget = false;
    [SerializeField] private bool aimOpposite = false;
    [SerializeField] private float reloadTime = 0.5f;

    [SerializeField] private bool enableSpread;
    [SerializeField] private int numberOfEnemySpawning = 2;
    [SerializeField] private float enemySpread = 90;



    private float timer;
    private float tangle = 999999;
    private float[] rotations;
    private Quaternion temp;
    private Quaternion rotation;

    // Start is called before the first frame update
    void Start()
    {
        if (!Target)
            Target = this.gameObject;
        if (Target.name == "Player")
            Target = GameObject.Find("Player");
        timer = reloadTime;
        temp = this.transform.rotation;
        //pfEnemy.GetComponent<MoveToTarget>().setTarget(Target);
    }

    // Update is called once per frame
    void Update()
    {

        //rotate to the target of you have one
        if (Target && aimTarget)
        {
            Quaternion targetRotation = Quaternion.LookRotation(Target.transform.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 1 * Time.deltaTime);

        }
        if (timer <= 0)
        {
            SpawnBullets();
            timer = reloadTime;
        }
        timer -= Time.deltaTime;

    }
    public void SpawnBullet()
    {
        GameObject spawnedEnemy = Instantiate(pfEnemy, this.transform.position, temp);

    } //Spawn your bullet
    public void SpawnBullets()
    {
        if (enableSpread)
        {
            if (numberOfEnemySpawning < 2)
                numberOfEnemySpawning = 2;
            for (int j = 0; j != numberOfEnemySpawning; j++)
            {
                rotations = DistributedRotations();
                temp.eulerAngles = new Vector3(0, 0, this.transform.rotation.eulerAngles.z - rotations[j]);

                SpawnBullet();
            }
        }
        else
        {
            temp.eulerAngles = new Vector3(0, 0, this.transform.rotation.eulerAngles.z);
            SpawnBullet();
        }
    } // test if spread is enable to spawn bullets

    public float[] DistributedRotations()
    {
        float[] rotations = new float[numberOfEnemySpawning];
        for (int i = 0; i < numberOfEnemySpawning; i++)
        {
            var test = 1;
            if (enemySpread % 360 == 0)
                test = 0;
            var fraction = (float)i / ((float)numberOfEnemySpawning - test);
            var difference = (this.transform.rotation.z + enemySpread / 2) - (this.transform.rotation.z - enemySpread / 2);
            var fractionOfDifference = fraction * difference;
            rotations[i] = fractionOfDifference + (this.transform.rotation.z - enemySpread / 2);
        }
        return rotations;
    }
}
