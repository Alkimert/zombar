﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Raycast : MonoBehaviour
{
    [SerializeField] private GameObject origin;
    [SerializeField] private GameObject direction;
    // Start is called before the first frame update
    [SerializeField] private GameObject gun;
    Animator gunAnimator;
    void Start()
    {
        gunAnimator = gun.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
       if (Input.GetMouseButtonUp(0))
       {
            gunAnimator.SetTrigger("Shoot");
            RaycastHit hit;
            Debug.DrawRay(origin.transform.position, direction.transform.position - origin.transform.position, Color.red);
            if (Physics.Raycast(origin.transform.position, origin.transform.forward, out hit))
            {
                if (hit.transform.tag == "Enemy")
                    hit.collider.gameObject.SetActive(false);
            }
        }
    }
}
