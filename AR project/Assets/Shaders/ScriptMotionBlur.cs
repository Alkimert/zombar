﻿using UnityEngine;

[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class ScriptMotionBlur : MonoBehaviour {
    public Shader motionBlurShader;

    [Range(0.1f, 10.0f)]
    public float intensity = 1.0f;

    [Range(5, 50)]
    public int quality = 10;

    private Material motionBlurMaterial;
    private Matrix4x4 previousViewProjection;

    void OnRenderImage(RenderTexture source, RenderTexture destination) {
        if (motionBlurShader != null) {
            Camera cameraComponent = GetComponent<Camera>();
            Matrix4x4 viewProjection = cameraComponent.projectionMatrix * cameraComponent.worldToCameraMatrix;
            if (previousViewProjection == null) previousViewProjection = viewProjection;   
            if (motionBlurMaterial == null) motionBlurMaterial = new Material(motionBlurShader);
            motionBlurMaterial.SetMatrix("_ViewInverse", viewProjection.inverse);
            motionBlurMaterial.SetMatrix("_PrevView", previousViewProjection);
            motionBlurMaterial.SetFloat("_Intensity", intensity);
            motionBlurMaterial.SetInt("_Quality", quality);
            Graphics.Blit(source, destination, motionBlurMaterial);
            previousViewProjection = viewProjection;
        }
    }
}
